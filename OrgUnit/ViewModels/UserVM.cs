﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrgUnit.ViewModels
{
    public class UserVM
    {
        public int id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
    }
    
    public class UserLoginInputVM
    {
        public string login { get; set; }
        public string password { get; set; }
    }

    public class OrgUnitVM
    {
        public int id { get; set; }
        public string title { get; set; }
        public UserVM headUser { get; set; }
        public OrgUnitVM orgUnitParrent { get; set; }
        public List<UserVM> employees { get; set; }
        public List<OrgUnitVM> orgUnitChilds { get; set; }
    }

    public class OrgUnitCreateVM
    {
        public string title { get; set; }
        public int? parrentOrgUnitId { get; set; }
    }

    public class OrgUnitAddHeaderVM
    {
        public int orgUnitId { get; set; }
        public int userId { get; set; }
    }

    public class OrgUnitAddEmployeeVM
    {
        public int orgUnitId { get; set; }
        public int userId { get; set; }
    }

    public class OrgUnitRemoveEmployeeVM
    {
        public int orgUnitId { get; set; }
        public int userId { get; set; }
    }



}